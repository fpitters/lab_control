@echo off
setlocal enabledelayedexpansion
PUSHD \\%~P0

REM General options
SET sensor=HPK_8in_192ch_200_Z3414_3
SET split=200um,~20~degC,~scratch~on~backside~of~197
SET geo=hex_positions_HPK_198ch_8inch_testcap.txt
SET colorpalette=--colorpalette 53
SET textcolor=--textcolor 8
SET invert_x=--invertx
SET display_help=0
SET collect_data=0
SET doCurrentDevelopmentGif=0

REM IV options
SET voltage_IV=-800
SET measurement_IV=Cell~current
SET iv_file=iv.dat
SET unit_IV=--ys -1e9
SET label_IV=--vn current:I:nA
SET doCellCurrentDevelopment=0
SET range_IV_details=-z 0.1:10
SET range_IV_details_x=
SET range_IV_HexPlot=-z 0.1:250
SET range_IV_cell_development=%range_IV_details%
SET format_IV=--if SELECTOR:no:PADNUM:VAL:no:no:no:no:no:no:no
SET basic_command_IV=HexPlot.exe -g %geo% %unit_IV% %label_IV% -i %iv_file% %colorpalette% %textcolor% --info tl:%measurement_IV%:tr:%sensor%:ll:%split% %format_IV%

REM Bias options
SET voltage_bias=%voltage_IV%
SET measurement_IV_bias=Bias~current
SET bias_file=iv.dat
SET unit_IV_bias=--ys -1e3
SET label_IV_bias=--vn current:I:mA
SET doBiasCurrentDevelopment=0
SET range_bias_details=
SET range_bias_HexPlot=
SET range_bias_details_x=
SET range_IV_bias_development=%range_bias_details%
SET format_IV_bias=--if SELECTOR:no:PADNUM:no:no:VAL:no:no:no:no:no
SET basic_command_IV_bias=HexPlot.exe -g %geo% %unit_IV_bias% %label_IV_bias% -i %bias_file% %colorpalette% %textcolor% --info tl:%measurement_IV_bias%:tr:%sensor%:ll:%split%  %format_IV_bias%


REM Remove detailed pdf files
del *details*.pdf
REM delete temporary pdf files
DEL tmp_*

REM ----------------------------------------------------------------------------
REM ------------------------ Generate Plots ------------------------------------
REM ----------------------------------------------------------------------------

REM IV
%basic_command_IV% --select %voltage_IV% -o tmp_plot.pdf                                 %range_IV_details% %range_IV_details_x% --od . %invert_x% --detailed
%basic_command_IV% --select %voltage_IV% -o %sensor%_current_at_%voltage_IV%V_values.pdf %range_IV_HexPlot% --pn 1 --nd 2
%basic_command_IV% --select %voltage_IV% -o %sensor%_current_at_%voltage_IV%V_cells.pdf  %range_IV_HexPlot% --pn 0

REM IV Manipulate detailed pdfs
DEL *current_overlay*
REN tmp_plot_details_all.pdf %sensor%_current_overlay.pdf
REN tmp_plot_details_all_heat.pdf %sensor%_current_overlay_heat.pdf
pdftk tmp_plot_details_*to* cat output %sensor%_current_details_per_cell.pdf
DEL current_details_*to*
DEL current_details_all*

REM Bias
%basic_command_IV_bias% --select %voltage_bias% -o tmp_plot.pdf                                        %range_bias_details%  %range_bias_details_x% --od . %invert_x% --detailed --nolog
%basic_command_IV_bias% --select %voltage_bias% -o %sensor%_bias_current_at_%voltage_bias%V_values.pdf %range_bias_HexPlot% --pn 1 --nd 3
%basic_command_IV_bias% --select %voltage_bias% -o %sensor%_bias_current_at_%voltage_bias%V_cells.pdf  %range_bias_HexPlot% --pn 0

REM Bias Manipulate detailed pdfs
DEL *bias_overlay*
REN tmp_plot_details_all.pdf %sensor%_bias_overlay.pdf
REN tmp_plot_details_all_heat.pdf %sensor%_bias_overlay_heat.pdf
pdftk tmp_plot_details_*to* cat output %sensor%_bias_details_per_cell.pdf
DEL bias_details_*to*
DEL bias_details_all*


REM --------------------------current development-------------------------------------
SET doCurrentDevelopment=0

IF "%doCellCurrentDevelopment%" == "1" (
	SET doCurrentDevelopment=1
	)

IF "%doBiasCurrentDevelopment%" == "1" (
	SET doCurrentDevelopment=1
	)

IF "%doCurrentDevelopment%" == "1" (
	echo ""
	echo Assembling current development
	SET commentchar=#
	SET minuschar=-

	FOR /F "tokens=1" %%A IN (!iv_file!) DO (
		SET str1=%%A
		SET firstchar=!str1:~0,1!
		if NOT !firstchar! == !commentchar! (
			REM SET v=!str1:~0,-7!
			SET v=!str1!
			REM echo %%A
			REM echo !str1!
			REM echo v = !v!

			REM generate zero-padded string for temporary file name
			SET firstchar=!v:~0,1!
			IF !firstchar! == !minuschar! (
				SET tmp_v=!v:~1!
				SET tmp_v=000!tmp_v!
				SET tmp_v=!tmp_v:~-7!
				SET tmp_v=-!tmp_v!
				)
			IF NOT !firstchar! == !minuschar! (
				SET tmp_v=000!tmp_v!
				SET tmp_v=!tmp_v:~-7!
				)
			REM echo tmp_v = !tmp_v!
			IF NOT EXIST tmp_cell_numbers_!tmp_v!.pdf (
				IF "%doCellCurrentDevelopment%" == "1" (
					echo --------------------------------- Generating cell current pdf file of voltage !v! ---------------------------------
					%basic_command_IV% -o tmp_cell_numbers_!tmp_v!.pdf %range_IV_cell_development% --pn 0 --select !v!
					%basic_command_IV% -o tmp_cell_values_!tmp_v!.pdf %range_IV_cell_development% --pn 1 --nd 1 --select !v!
					)
				)
			IF NOT EXIST tmp_bias_numbers_!tmp_v!.pdf (
				IF "%doBiasCurrentDevelopment%" == "1" (
					echo --------------------------------- Generating bias current pdf file of voltage !v! ---------------------------------
					%basic_command_IV_bias% -o tmp_bias_numbers_!tmp_v!.pdf %range_IV_bias_development% --pn 0 --select !v!
					%basic_command_IV_bias% -o tmp_bias_values_!tmp_v!.pdf %range_IV_bias_development% --pn 1 --nd 1 --select !v!
					)
				)
			)
		)

		echo ""
	echo Generating pdf output
	IF "%doCellCurrentDevelopment%" == "1" (
		pdftk tmp_cell_numbers* cat output %sensor%_cell_current_development_cells.pdf
		pdftk tmp_cell_values* cat output %sensor%_cell_current_development_values.pdf
		)
	IF "%doBiasCurrentDevelopment%" == "1" (
		pdftk tmp_bias_numbers* cat output %sensor%_bias_current_development_cells.pdf
		pdftk tmp_bias_values* cat output %sensor%_bias_current_development_values.pdf
		)
)

IF "%doCurrentDevelopmentGif%" == "1" (
	echo ""
	echo Generating animated gif output
	IF "%doCellCurrentDevelopment%" == "1" (
		convert -delay 50 -density 200 tmp_cell_numbers* %sensor%_cell_current_development_cells.gif
		convert -delay 50 -density 200 tmp_cell_values* %sensor%_cell_current_development_values.gif
		)
	IF "%doBiasCurrentDevelopment%" == "1" (
		convert -delay 50 -density 200 tmp_bias_numbers* %sensor%_bias_current_development_cells.gif
		convert -delay 50 -density 200 tmp_bias_values* %sensor%_bias_current_development_values.gif
		)
	echo Done
)

REM delete temporary pdf files
DEL tmp_*


IF "%display_help%" == "1" (
	HexPlot.exe -h
	pause
)
