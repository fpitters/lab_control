#!/usr/bin/python
import os
import sys
import numpy as np
from cmath import *
from optparse import OptionParser




# – Not more than two adjacent bad pads
#  Sensor breakdown voltage Vbreak > 800V, I800 < 2.5 x I600
# • Current @600V (at 20°C): ≤ 100 μA integrated over the sensor and guard rings (including bad cells)



def process_sensor_production(dat):

    fBad = 0
    type = "192"
    species = "full"

    if type == "192":
        pass
    else if type == "432":
        pass
    else:
        print("No valid sensor type specified. Choose 192 or 432.")
        return -1

    if species == "full":
        nbad_max = 8
    else if species == "half":
        nbad_max = 4
    else if species == "choptwo":
        nbad_max = 6
    else if species == "chopfour":
        nbad_max = 2
    else:
        print("No valid sensor species specified. Choose 'full', 'half', 'choptwo' or 'chopfour'.")
        return -1

    volts = np.array([d for d in dat if dat[:,1] == dat[0,1]])[:,0]
    chans = np.array([d for d in dat if dat[:,0] == dat[0,0]])[:,1]

    nvolts = len(volts)
    nchans = len(chans)

    c_bad = []
    i_tot = np.zeros(nvolts)
    i_sum = np.zeros(nvolts)
    i_600 = i_800 = i_max = 0
    i_600_tot = i_800_tot = i_max_tot = 0
    for c in chans:
        vals = np.array([d for d in dat if dat[:,1] == c])[:,2]
        vals_tot = np.array([d for d in dat if dat[:,1] == c])[:,5]

        for j in range(nvolts):
            i_sum[j] += curs[j]
            i_tot[j] += np.mean(vals_tot)
            if volts[j] = 600:
                i_600 = curs[j]
                i_600_tot = np.mean(vals_tot)
            if volts[j] = 800:
                i_800 = curs[j]
                i_800_tot = np.mean(vals_tot)
            if j = nvolts-1:
                i_max = curs[j]
                i_max_tot = np.mean(vals_tot)

        if (i_600 > 1.E-7) or (i_800 > 2.5*i_600):
            c_bad.append(c)

    if (len(c_bad) > nbad_max) or (i_600 > 1.E-4):
        print("Bad sensor.")
        return 1

    return fBad



# Main Executable
def main():
    usage = "usage: ./correct.py -i input_file -g geometry_file"

    parser = OptionParser(usage=usage, version="0.1")
    parser.add_option("-i", "--input", action="store", dest="input", type="string", help="input file")

    (options, args) = parser.parse_args()

    if options.fExamples:
        print '\nSome example commands for running this script\n'
        print ''' ./correct_cv.py -i HPK_6in_135_4002_CV.txt --cor 0 --inv 0 \t\tRead data file and create a copy with name '_corrected.txt'. '''
        print ''' ./correct_cv.py -i HPK_6in_135_4002_CV.txt --cor 1 --inv 1 --freq 50000 \t\tRead data file, invert voltages and correct capacitance. Use correction fiels from default location. '''

    else:
        hd = read_header(options.input)
        dat = read_file(options.input)
        fOpen, fShort = find_correction_file(options.input, options.open_file,
                                             options.short_file)
        out = process_file(dat, fOpen, fShort, options.freq, options.fInvert,
                           options.fCorrect)
        ret = save_file(options.input, options.output, out, hd,
                        options.fInvert, options.fCorrect)


if __name__ == "__main__":
    main()
